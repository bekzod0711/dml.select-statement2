SELECT
    store_id,
    staff_id,
    MAX(total_revenue) AS highest_revenue
FROM
    (SELECT
        store_id,
        staff_id,
        SUM(amount) AS total_revenue
    FROM
        payment
    WHERE
        YEAR(payment_date) = 2017
    GROUP BY
        store_id,
        staff_id) AS store_revenue
GROUP BY
    store_id;
SELECT
    film.title AS movie_title,
    COUNT(rental.rental_id) AS rental_count,
    AVG(customer.age) AS expected_age
FROM
    film
JOIN
    inventory ON film.film_id = inventory.film_id
JOIN
    rental ON inventory.inventory_id = rental.inventory_id
JOIN
    customer ON rental.customer_id = customer.customer_id
GROUP BY
    film.title
ORDER BY
    rental_count DESC
LIMIT
    5;
SELECT
    film.title AS movie_title,
    COUNT(rental.rental_id) AS rental_count,
    AVG(customer.age) AS expected_age
FROM
    film
JOIN
    inventory ON film.film_id = inventory.film_id
JOIN
    rental ON inventory.inventory_id = rental.inventory_id
JOIN
    customer ON rental.customer_id = customer.customer_id
GROUP BY
    film.title
ORDER BY
    rental_count DESC
LIMIT
    5;
